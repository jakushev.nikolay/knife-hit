using UnityEngine;

public class Knife : MonoBehaviour
{
    [SerializeField] private float _speed;
    private Rigidbody _rigidbody;
    private Spawner _spawner;

    private bool onWood = false;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _spawner = GameObject.Find("Knife Spawner").GetComponent<Spawner>();
    }

    private void Update()
    {
        PressButton();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Wood")
        {
            gameObject.transform.SetParent(other.transform);
            _rigidbody.velocity = Vector3.zero;
            onWood = true;
            _rigidbody.detectCollisions = false;

            _spawner.SpawnKnife();
            _spawner.IncrementScore();
        }

        if(other.tag == "Knife")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
    }

    private void PressButton()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !onWood)
        {
            _rigidbody.velocity = new Vector3(0f, _speed, 0f);
        }
    }

}
