using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Transform _spawn;
    [SerializeField] private GameObject _spawnPrefab;

    private Text scoreText;
    private int score;

    private void Awake()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
    }

    public void SpawnKnife()
    {
        GameObject knifeSpawn = Instantiate(_spawnPrefab, _spawn.position, _spawn.rotation);
        knifeSpawn.transform.parent = _spawn;
    }

    public void IncrementScore()
    {
        score++;
        scoreText.text = score.ToString();
    }

}