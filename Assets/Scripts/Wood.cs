using UnityEngine;

public class Wood : MonoBehaviour
{
    [SerializeField] private float speed;

    void Update()
    {
        transform.Rotate(0f, speed, 0f);
    }
}
